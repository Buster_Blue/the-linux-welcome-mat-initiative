# What you should know 
While TLWMI is aimed at the average computer user, you should be familiar with some basic terms and concepts before you start following any TLWMI guides or content. If you understand the concepts and terms laid out here, and still find a guide difficult to follow or understand, you should raise an issue on our [BitBucket repository](https://bitbucket.org/charlesdaniels/the-linux-welcome-mat-initiative/issues?status=new&status=open), or send us an email at tlwmi_dev[at]mm[dot]st. 

If anything seems unfamiliar, don't worry, everything has links to relevant learning materials to help you brush up. 

# Required knowledge 
This section contains knowledge you must know in order to utilize TLWMI guides and materials.

## You should be able to define the terms...  
* [Storage device](http://techterms.com/definition/storagedevice)
* [Hard disk](http://techterms.com/definition/harddisk)
* [Optical media](http://techterms.com/definition/opticalmedia)
* [USB flash drive](http://techterms.com/definition/flashdrive)
* [Operating system](http://techterms.com/definition/operating_system)
* [boot](https://simple.wikipedia.org/wiki/Booting)
* [login](https://en.wikipedia.org/wiki/Login)
* [application](http://searchsoftwarequality.techtarget.com/definition/application-program)
* [user account](http://www.encyclopedia.com/doc/1O11-useraccount.html)


## You should understand the concepts...

* [The difference between a file and a folder](http://www.pcworld.com/article/187188/Raise_Your_Windows_IQ_The_Difference_Between_Files_and_Folders.html)

## Common jargon and slang 
| term            | common jargon/slang             |
|-----------------|---------------------------------|
|Storage Device   | disk, drive                     |
|Hard Disk        | HDD, spinning disk              |
|USB Flash Drive  | flash drive, stick, thumb drive |
|Operating System | OS, system                      |
|login            | sign in, authenticate           |
|application      | app, program, executable        |
|user account     | user                            | 


Keep in mind, as with most varieties of slang and jargon, many words may mean several things (eg. "user" could refer to the person using a computer, or to a user account), use context clues or ask for clarification if you are unsure! 

# Recommended knowledge 
This section contains knowledge that is recommend, but not required to understand TLWMI guides and materials. 

## Terms 
* [kernel](http://www.go4expert.com/articles/operating-kernel-types-kernels-t24793/)
* [UNIX-like](https://en.wikipedia.org/wiki/Unix-like)
* [BIOS](http://whatis.techtarget.com/definition/BIOS-basic-input-output-system)
* [POST](http://whatis.techtarget.com/definition/POST-Power-On-Self-Test)

## Concepts 
* [File sizes](http://www.computerhope.com/issues/chspace.htm) and [SI prefixes](https://en.wikipedia.org/wiki/File_size)


# Optional knowledge
This section contains knowledge that may be interesting, or may be useful to more advanced users. You should be able to understand all TLWMI content without knowing any of this. 