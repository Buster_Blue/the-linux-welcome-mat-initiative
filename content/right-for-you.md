# Introduction
In this article, I will explore the pros and cons of using Linux as your primary operating system. While Linux offers a phenomenal tool to take ownership of one's computing experience, it is not right for everyone. Here, we will discuss how to decide if Linux is right for you. 

## The Linux mindset
Linux is more than just an operating system, it is a community, and a philosophy. While many people have tried to define the Linux philosophy, and many will in the future, this is how I like to think of it:

* User choice is king - the real beauty of Linux is giving the end user the freedom to have the computing experience they want, rather than letting some company define the computing experience for the user. 

* Community is also king - the open source community is powerful because **we** enable our users and developers, because we **are** the users and developers. It is up to us, as a community to make Linux what we want it to be, and to help other find their place in the world of computing. 

# Why you might want to use Linux 
In this section, we will discuss the benefits of Linux, both objective and subjective. 

## Performance 
**Section under development** 

## Openness 
Linux is [open](http://www.bcs.org/content/ConWebDoc/3003) [source](http://www.howtogeek.com/129967/htg-explains-what-is-open-source-software-and-why-you-should-care/), meaning the source code which defines the Linux kernel, as well as nearly every component of your desktop experience is available for anyone to download, modify, and re-distribute. This is how open-source applications like Linux are made - a developer sees a need, and writes a simple application. Other developers like their application, and modify and add on to it, then send their code back to the original developer (or release a derivative project). Thus has almost every major open source project been created. 

## Security 
Open source software is generally [more](http://opensource.com/business/15/5/why-open-source-means-stronger-security) [secure](http://venturebeat.com/2013/11/26/linux-chief-open-source-is-safer-and-linux-is-more-secure-than-any-other-os-exclusive/) than closed source software alternatives. Companies like [Google](https://en.wikipedia.org/wiki/Google_platform#Software) and [Facebook](http://royal.pingdom.com/2010/06/18/the-software-behind-facebook/) use Linux to power their servers - just like [~75% of the enterprise systems](http://www.theinquirer.net/inquirer/news/2385329/linux-is-winning-enterprise-cloud-market-share-at-expense-of-windows). Companies of this caliber do not use insecure systems to build their businesses. 

## User freedom 
Linux offers end users much more freedom than Windows or OSX can. A Linux system can be whatever you want it to be. Every aspect of the system can be customized however you like, from desktop themes, to application choices. Or, you can use one of the many "user friendly" distros and let someone else do the choosing for you, if you prefer. 

# Why you might not want to use Linux 
In this section, we will discuss the negative aspects of Linux, objective and subjective. 

## You rely on Windows-specific software
While Linux has a very complete library of software available, not all applications will run on Linux. If you rely on tools, such as CAD software, or Adobe products, Linux may not be for you. While WINE is available, and offers a limited amount of compatibility, application support often lags years behind Windows. Additionally, games who like Windows-only games will likely be disappointed. While the library of games available on Linux is rapidly growing, it is still far behind Windows. 

## You are happy with your computing experience already
Linux is about user freedom to create or choose a computing experience that works best for the end user. If you use Windows or OSX, and you are completely satisfied with your computing experience as it stands, Linux may not be for you. It makes little sense to learn a new interfaces, and new programs, if you are totally satisfied with what you have already. 

## You want a Windows replacement 
[Linux is not Windows](http://linux.oneandoneis2.org/LNW.htm)! If you are looking for something that behave just like Windows, you will be sorely disappointed. While I find that Linux offers a superior alternative to most aspects of Windows, not everyone agrees. If you are looking for "Windows, but free", you should probably look at [RactOS](https://reactos.org/). 

## You expect timely, on-call tech support 
Unlike Windows, Linux and open source projects are not made by a for-profit company. There is not Linux tech support hotline (although some companies offer Linux tech support for a fee). While there are many resources available for getting help with Linux problems, almost all "free" tech support you will receive regarding Linux and other FOSS software is provided by volunteers. Volunteers generally do not take kindly to disrespect or impatience. 

# Conclusion

If you have decided Linux is right for you, check out the [fist steps](content/frist-steps.md) page, where we discuss first steps for switching to Linux. If you have decided Linux is not for you, I respect your choice, but I hope I have given you the information needed to constantly re-evaluate your choice in software, and make educated decisions about your computing experience every day. 

# Closing notes 
This is an [open source project](https://github.com/charlesdaniels/linux-guide), and as such, suggestions and modifications are welcome. If you notice an error, or have something to add, speak up in a comment, or submit a pull request on the github page! 


-------------

This work is licensed as [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/legalcode). 
