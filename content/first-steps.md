# I want to switch to Linux, what now? 
So you have decided that Linux is right for you, now what? In this article, we will discuses the first steps to making your transition. The world of Linux is large, and it can often times be difficult to figure out how to start. 


# Before we begin, a short word on backups
When installing any operating system, or making any major system changes, you should always have at least one, but ideally two backups. Even if you are doing something that is supposed to be safe and tested, you should still have a backup. Modifying your primary system with live, un-backed up data is a recipe disaster; the data-gremlins usually strike at the most inopportune times. 

Even if you are not installing Linux at all, or if you are installing Linux on a separate hard drive, you should always have effective backups anyway. If you are an OSX user, this is as simple as setting up an external hard drive for Time Machine backups. For Windows users, even simply copying all your files to an external hard drive can do the trick. 

If you do not already have a backup solution, my advice would be to go buy two external hard drives, make a backup on both. Then, keep on in a safe place in your home, and mail the other to your parents/grandparents/best friend/etc. for safe keeping. I recommend this regardless of OS choice - the data-gremlins plague all OSes equally. 

That said, you probably will not need to use your backups. Thats why they call them backups. The Linux installation process is very well tested and safe. Even if you are dual-booting with Windows and resizing an existing partition, you will most likely not have to use your backups. Nevertheless, it does not pay to gamble with your data. 

# Testing out distros with a liveCD or liveUSB 
**section under development** 

# Selecting a distro 
Selecting the right distro is one of the most important decisions you will make during your Linux career. Don't sweat over it too much, you can always re-install another distro later (one more reason to keep effective backups). 

## I value stability, security, and long term support
Use [Debian](https://www.debian.org/). Debian has one of the largest collections of software packages of any distro, and Debian packages are of consistently high quality. Each Debian release has a long support cycle, and is usually supported for several years. The trade off is that Debian often has older packages than other distros, opting to wait until a new version has been tried and tested before pushing it out to users. Check out the [official installation guide](https://www.debian.org/releases/stable/amd64/index), or this [unofficial guide](https://linuxpanda.wordpress.com/2014/03/02/guide-how-to-install-debian-8-jessie-xfce-using-netinst-minimal-iso-step-by-step-with-pictures/). 

## I value popularity and beginner friendliness 
Use [Ubuntu](http://www.ubuntu.com/) or a derivative like [Kubuntu](http://www.kubuntu.org/) or [Xubuntu](http://xubuntu.org/). Ubuntu often has newer packages than Debian, but packages are often not as well tested as those in Debian. Additionally, updates are often held off until the next numbered release, rather than released on the fly (*backported*), which can leave users with outdated or broken software packages until the next release. This is not to say that Ubuntu is unstable, or that your software will break often, but rather that stability is not the top priority, ease of use is. 

The real benefit of Ubuntu is 3rd party support (eg. only Ubuntu is officially supported by Steam for Linux). The other key advantage is popularity, you are far more likely to find relavant results on Google about Ubuntu as opposed to say, Slackware. Review the [official installation guide](http://www.ubuntu.com/download/desktop/install-ubuntu-desktop) for help installing! 

If you like Ubuntu, and are satisfied with it's package quality, you may also wish to considr [Linux Mint](http://www.linuxmint.com/) which features either the Cinnamon or Mate desktop environment, both of which will be very familiar to Windows users. Linux Mint uses the Ubuntu package repositories, but features some in-house extras to improve the ease-of-use. For installation instructions, you can review the [official guide](http://www.linuxmint.com/documentation.php). 

## I value ease-of-use **and** stability 
Use [Linux Mint Debian Edition (LMDE)](http://www.linuxmint.com/download_lmde.php). LMDE combines the Debian operating system, with the ease of use and interface of Linux Mint (MATE or Cinnamon, I suggest Cinnamon for transitioning Windows users). Cinnamon is very familiar to the Windows 7 interfaces, and people who liked the Windows 7 start menu will be right at home. If you do not feel confident installing and using Debian, use LMDE; fundamentally LMDE **is** Debian, just prettier. Check out [this guide](https://hreikin.wordpress.com/2013/12/20/lmde-linux-mint-debian-edition-install-guide/) to install it. 

## I like to learn things the hard way, and I am okay with breaking everything a few times
**ATTENTION**: Please do not use Arch if are not willing to scale a learning cliff, if you are frustrated easily, or want something that "just works out of the box". Also, for the love of god do not install Arch on your primary (or even worse, only) computer and expect to get it right the first time! 

[ArchLinux](https://www.archlinux.org/) may just be right for you. Arch is a minimal distro, not even shipping with a GUI. It allows an incredible level of flexibility and customizeability, beyond what Debian, Mint, or Ubuntu can offer. The trade off is that understanding how your OS works at a low level is a requirement for use. Arch is not an "easy" distro, but the payoff is often worth it for the patient and open minded. Bear in mind, your first install will almost definitely be broken. Your second one too. But stick with it and and you will never want to go back. Check out the [ArchWiki's beginners' guide](https://wiki.archlinux.org/index.php/Beginners%27_guide) for installation instructions. 


## Honorable Mentions 
While I feel the above represent the best of distros for beginners, the following deserve honorable mention. I encourage all beginners to try out many distros, including the below, using liveCDs or USBs. 

* [OpenSUSE](https://www.opensuse.org/en/) 
* [Fedora](https://getfedora.org/)
* [Manjaro](https://manjaro.github.io/) 
* [Mageia](http://www.mageia.org/en/) 

# Dual booting Linux with Windows
Dual booting gives you the best of both worlds, allowing you to choose either Linux or Windows each time you boot your PC. There are many approaches and methods, here are some to consider:

* [Instructables - How to Dual-boot Linux and Windows (on a PC with Windows 7 already installed)](http://www.instructables.com/id/How-to-Dual-boot-Linux-and-Windows-on-a-PC-with-W/)
* [Ubuntu Community Help Wiki - Windows Dual Boot](https://help.ubuntu.com/community/WindowsDualBoot)
* [ArchWiki - Windows and ARch dual boot](https://wiki.archlinux.org/index.php/Windows_and_Arch_dual_boot)
* [How-To Geek - How to Dual Boot Linux on Your PC](http://www.howtogeek.com/214571/how-to-dual-boot-linux-on-your-pc/)

-----------

This work is licensed as [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/legalcode). 
