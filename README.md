# Project on Hiatus
This project is on indefinite Hiatus, as I do not have time to maintain it. If you are interested in taking over this project, please message me on BitBucket. 

# The Linux Welcome Mat Initiative 
*Inform, Advise, Assist* 

## Our content 
[Is Linux right for me?](content/right-for-you.md)

[Switching to Linux - first steps](content/first-steps.md)

## About

**TLWMI is still in an early stage of development, and a lot of the content we want to have has not yet been written** 

# That TLWMI is 
TLWMI seeks to inform computer users of all options available to them, so they can select the environment and platform that best suits their computing needs. It is our objective to provide accurate, unbiased information to users considering switching to Linux, and to provide helpful and accurate information to those who have decided to switch, but do not know where to start.

TLWMI aims to target the widest possible range of computer users, but in particular is meant to be friendly to the average end-user. To use the resources of TLWMI, as little as possible computing experience is required.  

# What TLWMI is not 
TLWMI is not "marketing for Linux". We are not trying to sell anything, let alone Linux or open source. We are not here to push the "free software" agenda, only to inform users of their choices, advise them on what choice may best for them, and assist in the transition to the new platform. 

# Content we need 
Content that needs to be added is now kept in the [issue tracker](https://bitbucket.org/charlesdaniels/the-linux-welcome-mat-initiative/issues?status=new&status=open).  

# How to contribute to TLWMI 
If you already know how to use git, just submit a pull request. If not, you can send me a PM on Voat or Bitbucket, and I will add your content, and your name or handle as a contributor. No contribution is too small, even something as simple as fixing spelling or formatting is a great help. 

## Standards and practices 
All TLWMI content should be written in Markdown or LaTeX format, and distributed as both source and pdf. All TLWMI content should have a relevant [license banner](https://creativecommons.org/choose/). All headers should have their first word capitalized, and any proper nouns should follow their normal capitalization regardless of position within the header. All TLWMI content should be kept under source control, in this git repository. 

That said, any content is better than no content. It is preferable to lay down core content first, then go back and fix formatting later. 


------------


This work is licensed as [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/legalcode). 